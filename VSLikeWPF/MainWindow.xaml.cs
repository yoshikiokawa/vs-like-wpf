﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VSLikeWPF
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
			StateChanged += MainWindow_StateChanged;
		}

		private void MainWindow_StateChanged(object sender, EventArgs e)
		{
			Window window = ((Window)sender);
			if (window.WindowState == System.Windows.WindowState.Maximized)
			{
				IntPtr handle = new WindowInteropHelper(window).Handle;

				var screen = System.Windows.Forms.Screen.FromHandle(handle);
				if (screen.Primary)
				{
					mainGrid.Margin = new Thickness(
						6 + SystemParameters.WorkArea.Left,
						6 + SystemParameters.WorkArea.Top,
						6 + (SystemParameters.PrimaryScreenWidth - SystemParameters.WorkArea.Right),
						6 + (SystemParameters.PrimaryScreenHeight - SystemParameters.WorkArea.Bottom));
					mainBorder.BorderThickness = new Thickness(0);
					dropShadowEffect.Opacity = 0;
				}
				else
				{
					mainGrid.Margin = new Thickness(6);
				}
				logo.Margin = new Thickness(5,5,0,0);
				logo.Width = 25;
				logo.Height = 28;
				windowChrome.ResizeBorderThickness = new Thickness(0);
				windowChrome.CaptionHeight = 28 + 6;
			}
			else
			{
				mainGrid.Margin = new Thickness(28,28,7,7);
				mainBorder.BorderThickness = new Thickness(1);
				dropShadowEffect.Opacity = 1.0;
				logo.Margin = new Thickness(-20, -20, 0, 0);
				logo.Width = 50;
				logo.Height = 55;
				windowChrome.ResizeBorderThickness = new Thickness(28, 28, 7, 7);
				windowChrome.CaptionHeight = 28;
			}
		}
	}
}
